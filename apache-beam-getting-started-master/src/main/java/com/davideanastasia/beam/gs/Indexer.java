package com.davideanastasia.beam.gs;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.davideanastasia.beam.gs.fn.ReadFileFn;

public class Indexer {

	private static final Logger logger = LoggerFactory.getLogger(Indexer.class);

//	public static interface Options extends PipelineOptions {
//
//		@Description("Path of input files")
//		@Default.String("gs://dtlw/sabre_la_tcn_airline_20181004_0450.txt")
//		String getInputFile();
//
//		void setInputFile(String value);
//
//		@Description("path of output files")
//		@Default.String("/dtlw/out")
//		String getOutputFile();
//
//		void setOutputFile(String value);
//
//	}
//
//	public static void main(String[] args) throws Exception {
//
//		Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);
//		Pipeline p = Pipeline.create(options);
//
//		p.apply(FileIO.match().filepattern(options.getInputFile())).apply(FileIO.readMatches())
//				.apply("ReadFile", ParDo.of(new ReadFileFn()))
//				.apply("FormatOutput",
//						MapElements.into(TypeDescriptors.strings()).via(item -> item.getKey() + ": " + item.getValue()))
//				.apply(TextIO.write().to(options.getOutputFile()));
//		p.run().waitUntilFinish();
//	}
//	
	public interface WordCountOptions extends PipelineOptions {
		@Description("Path of the file to read from")
		ValueProvider<String> getInputFile();

		void setInputFile(ValueProvider<String> value);

		@Description("Path of the file to write to")
		ValueProvider<String> getOutput();

		void setOutput(ValueProvider<String> value);
	}

	public static void main(String[] args) {
		WordCountOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(WordCountOptions.class);
		Pipeline p = Pipeline.create(options);
		p.apply(FileIO.match().filepattern(options.getInputFile())).apply(FileIO.readMatches()).apply("ReadFile",
				ParDo.of(new ReadFileFn()));
//				.apply(MapElements.via(new FormatAsTextFn()))
//				.apply("WriteCounts", TextIO.write().to(options.getOutput()));

		p.run().waitUntilFinish();
	}

}
