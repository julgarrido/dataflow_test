package com.davideanastasia.beam.gs.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


public class RestClient {
	
	private RestTemplate restTemplate = new RestTemplate();
	private String endpoint;
	
	
	
	public RestClient(String endpoint) {
		this.endpoint = endpoint;
	}
	private static HttpHeaders defaultHeaders(MediaType mediaType) {
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(mediaType);
	    return headers;
	  }
	
	/**
	   * Method that performs a post towards services consumed. Receive additional
	   * headers for cases where https services must be executed.
	   * 
	   * @param endpoint
	   * @param additionalHeaders
	   * @return ResponseEntity
	   * @throws HttpClientErrorException
	   */
	  public ResponseEntity<String> checkService(String data) {

	    if (restTemplate == null) {
	      throw new IllegalStateException("Can't access to rest template");
	    }
	    HttpHeaders headers = defaultHeaders(MediaType.APPLICATION_JSON);	   

	    HttpEntity<Object> entity = new HttpEntity<>(data);
	    ResponseEntity<String> response = restTemplate
	    		 .exchange(endpoint, HttpMethod.POST, entity, java.lang.String.class);
	    return response;
	  }

}
