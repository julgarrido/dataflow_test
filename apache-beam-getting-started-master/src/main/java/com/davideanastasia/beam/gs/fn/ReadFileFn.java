package com.davideanastasia.beam.gs.fn;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.davideanastasia.beam.gs.Indexer;
import com.davideanastasia.beam.gs.entity.Metadata;

public class ReadFileFn extends DoFn<FileIO.ReadableFile, KV<Metadata, String>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6229956287291302739L;

	private static final Logger logger = LoggerFactory.getLogger(ReadFileFn.class);

	@ProcessElement
	public void processElement(ProcessContext c) {
		try {
			FileIO.ReadableFile f = c.element();
			String filename = f.getMetadata().resourceId().getFilename();
			logger.info("nombre file: " + filename);
//            RestClient restClient = new RestClient("http://localhost:8444/utilrest/rest/v1/echo");s

			ReadableByteChannel rbc = f.open();
			try (InputStream stream = Channels.newInputStream(rbc);
					BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {

				// we start counting rows from 1
				long lineId = 1;
				String line;
				while ((line = br.readLine()) != null) {
//                	if(StringUtils.isNoneEmpty(line)) {
//                		restClient.checkService(line);
//                	}                	

//                	if(line.startsWith("00292270709999919011222")) {
					System.out.println(line);
					c.output(KV.of(Metadata.of(filename, lineId), line));
//                	}

					lineId++;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}